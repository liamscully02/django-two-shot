# Generated by Django 4.1.5 on 2023-01-19 20:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("receipts", "0001_initial"),
    ]

    operations = [
        migrations.AlterField(
            model_name="receipt",
            name="tax",
            field=models.DecimalField(decimal_places=3, max_digits=10),
        ),
    ]
